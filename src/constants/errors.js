module.exports = {
    STARTDATE_BEFORE_CURRENT_CODE: 1,
    STARTDATE_BEFORE_CURRENT_ERROR: {
        message: "The startdate of the reservation has already passed! Select one in the future",
        code: 400
    },
    STARTDATE_AFTER_ENDDATE_CODE: 2,
    STARTDATE_AFTER_ENDDATE_ERROR: {
        message: "The enddate of the reservation is before or equal to the startdate! Select an endDate beyond the startdate!",
        code: 400
    },
    NO_RESERVATIONS_ERROR_FOR_USER: {
        message: "There are no reservations for this user",
        code: 500
    },
    NOT_ALLOWED_UPDATE_RES: {
        message: "You are not allowed to update this reservation",
        code: 401
    },
    NOT_ALLOWED_DELETE_RES: {
        message: "You are not allowed to delete this reservation",
        code: 401
    },
    PHONENUMBER_NOT_VALID: {
        message: "Phonenumber not valid",
        code: 400
    },
    POSTALCODE_NOT_VALID: {
        message: "Postalcode not valid",
        error: 400
    },
    EMAIL_NOT_VALID: {
        message: "Emailaddress not valid",
        code: 400
    },
    NO_ACCESS_EMAIL_NOT_VALID_OR_PASSWORD_NOT_CORRECT: {
        message: "No access: Emailaddress not found or password incorrect",
        code: 401
    },
    NOT_ABLE_CREATE_JWT: {
        message: 'Could not generate jwt',
        code: 500
    },
    AUTH_HEADER_MISSING: {
        message: 'Authorization header missing!',
        code: 401
    },
    NOT_AUTHORIZED: {
        message: 'not authorized',
        code: 401
    },
    USERID_MISSING: {
        message: 'UserId is missing!',
        code: 401
    },
    NO_APP_FOUND: {
        message: "No apartments found",
        code: 400
    },
    NOT_ALLOWED_UPDATE_APP: {
        message: "You are not allowed to update this apartment",
        code: 401
    },
    NOT_ALLOWED_DELETE_APP: {
        message: "You are not allowed to delete this apartment",
        code: 401
    },
    FIRSTNAME_NOT_VALID: {
        message: "Firstname is not valid",
        code: 400
    },
    LASTNAME_NOT_VALID: {
        message: "Lastname is not valid",
        code: 400
    },
    NO_RES_FOUND: {
        message: "No reservation found",
        code: 400
    }
};