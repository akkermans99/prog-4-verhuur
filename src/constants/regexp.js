module.exports = {
    phoneValidator: (phoneNumber) => {
        const phoneNumberVali = new RegExp('^06(| |-)[0-9]{8}$');
        if ( !phoneNumber ) {
            return false;
        }
        if (phoneNumber.trim() === '') {
            return false;
        }
        return Boolean(phoneNumber.match(phoneNumberVali));
    },
    emailValidator: (email) => {
        const emailVali = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if ( !email ) {
            return false;
        }
        if (email.trim() === '') {
            return false;
        }
        return Boolean(email.match(emailVali));
    },
    postalCodeValidator: (postalcode) => {
        const postalCodeVali = new RegExp("^[1-9][0-9]{3}[\\s]?[A-Za-z]{2}$");
        if ( !postalcode ) {
            return false;
        }
        if (postalcode.trim() === '') {
            return false;
        }
        return Boolean(postalcode.match(postalCodeVali));
    },
    nameValidator: (firstName) => {
        const firstNameVali = new RegExp(/^\D*$/g);
        if ( !firstName ) {
            return false;
        }
        if (firstName.trim() === '') {
            return false;
        }
        return Boolean(firstName.match(firstNameVali));
    }
}