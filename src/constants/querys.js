module.exports = {
    // select all apartments
    SELECT_ALL_APP: 'select * from Apartment',
    // select 1 apartment 
    SELECT_APP: (id) => {
        return `select * from Apartment where ApartmentId=${id}`;
    },
    // select all reservations
    SELECT_ALL_RESERVATIONS: (id) => {
        return `select * from Reservation where ApartmentId=${id}`;
    },
    // select 1 reservation
    SELECT_RESERVATION : (appID, resID) => {
        return `select * from Reservation where ApartmentId=${appID} AND ReservationId=${resID}`;
    },
    // insert apartment
    INSERT_APP: (description, streetAddress, postalcode, city, userId) => {
        return `insert into Apartment (Description, StreetAddress, PostalCode, City, UserId) values ('${description}', '${streetAddress}', '${postalcode}', '${city}', ${userId})`;
    },
    // update apartment
    UPDATE_APP: (description, streetAddress, postalcode, city, userId, appID) => {
        let obj = {
            'Description': description,
            'StreetAddress': streetAddress,
            'PostalCode': postalcode,
            'City': city
        };
        let condition = `ApartmentId=${appID} and UserId=${userId}`;
        return createUpdateQuery(obj, 'Apartment', condition);
    },
    // delete apartment
    DELETE_APP: (appID, userId) => {
        return `delete from Apartment where ApartmentId=${appID} and UserId=${userId}`;
    },
    // delete reservation
    DELETE_RES: (appID, resID, userId) => {
        return `delete from Reservation where ReservationId=${resID} and ApartmentId=${appID} and UserId=${userId}`;
    },
    // insert reservation
    INSERT_RES: (apartmentId, startDate, endDate, status, userId) => {
        return `insert into Reservation (ApartmentId, StartDate, EndDate, Status, UserId) values (${apartmentId}, '${startDate}', '${endDate}', '${status}', ${userId})`;
    },
    // update reservation
    UPDATE_RES: (status, appID, resID) => {
        let obj = {
            'Status': status
        };
        let condition = `ApartmentId=${appID} and ReservationId=${resID}`;
        return createUpdateQuery(obj, 'Reservation', condition);
    },
    // register query
    REGISTER: (firstName, lastName, streetAddress, postalcode, city, phoneNumber, emailAddress, dateOfBirth, password) => {
        return `insert into DBUser (FirstName, LastName, StreetAddress, PostalCode, City, PhoneNumber, EmailAddress, DateOfBirth, Password) values ('${firstName}', '${lastName}', '${streetAddress}', '${postalcode}', '${city}', '${phoneNumber}', '${emailAddress}', '${dateOfBirth}', '${password}')`;
    },
    // login query
    LOGIN: (emailAddress) => {
        return `select * from DBUser where EmailAddress='${emailAddress}'`;
    },
    // all users
    GET_ALL_USERS: `select * from DBUser`,
    // get all reservations of user
    GET_ALL_RESERVATIONS_USER: (userId) => {
        return `select * from Reservation where UserId=${userId}`;
    },
    // get all apartments of user
    GET_ALL_APARTMENTS_USER: (userId) => {
        return `select * from Apartment where UserId=${userId}`;
    },
    // get all apartments with owners and reservations
    GET_ALL_APP_WITH_USER_WITH_RES:
        `select(
            \tselect a.ApartmentId, a.City, a.Description, a.PostalCode, a.StreetAddress, a.UserId,
            \t(select * from DBUser as u where u.UserId = a.UserId for json path) as Owner,
            \t(select * from Reservation as r where r.ApartmentId = a.ApartmentId for json path) as Reservations
            \tfrom Apartment as a for json path
        \t) as Result;`,
    // get an apartment with owner and reservations
    GET_APP_WITH_USER_WITH_RES: (appId) => {
        return `select(
                    \tselect a.ApartmentId, a.City, a.Description, a.PostalCode, a.StreetAddress, a.UserId,
                    \t(select * from DBUser as u where a.UserId = u.UserId for json path) as Owner,
                    \t(select * from Reservation as r where a.ApartmentId = r.ApartmentId for json path) as Reservations
                    \tfrom Apartment as a
                    \twhere a.ApartmentId = ${appId} for json path
                ) as Result;`;
    },
    // get last added apartment
    GET_LATEST_APP: `select top(1) * from Apartment order by ApartmentId desc`,
    // get last added reservation from apartment with id appID
    GET_LATEST_RES: (appID) => {
        return `select top(1) * from Reservation where ApartmentId=${appID} order by ReservationId desc`;
    },
    GET_LATEST_USER: `select top(1) * from DBUser order by UserId desc`
};

/**
 * create update query, if a column does not needs to be updated then take old value
 * @param {OBJECT} obj 
 * @param {STRING} table 
 * @param {STRING} condition 
 */
function createUpdateQuery(obj, table, condition) {
    let query = `update ${table} set`;
    let count = 0;
    for( let key in obj ) {
        count++;
        if ( obj[key] ) {
            let val = obj[key];
            query += ` ${key}='${val}'`;
        } else {
            query += ` ${key}=${key}`;
        }

        if ( Object.keys(obj).length !== count ) {
            query += `,`;
        }
    }
    query += ` where ${condition}`;
    console.log(query);
    return query;
}