// require all standard packages
const express = require('express');
const router = express.Router();

// controller
const AuthController = require('../controllers/auth.controller');


/**
 * different endpoints for after /auth
 */
router.post('/register', AuthController.registerUser);
router.post('/login', AuthController.loginUser);
router.get('/users', AuthController.getAllUsers);

module.exports = router;