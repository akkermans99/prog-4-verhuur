// require all standard packages
const express = require('express');
const router = express.Router();

// controllers
const appController = require('../controllers/appartments.controller');
const resController = require('../controllers/reservations.controller');
const auth = require('../controllers/auth.controller');

/**
 * beneath all the different endpoints for after /api/appartments
 */
// get all apartments
router.get("/", appController.getAllWithOwner);

// add an apartment
router.post("/", auth.validateToken, appController.post);

// get an apartment
router.get("/:id", appController.getAppWithOwner);

// update an apartment
router.put("/:id", auth.validateToken, appController.put);

// delete an apartment
router.delete("/:id", auth.validateToken, appController.delete);

// add a reservation
router.post("/:id/reservations", auth.validateToken, resController.post);

// get all reservations of an apartment
router.get("/:id/reservations", resController.getAllRes);

// get a reservation of an apartment
router.get("/:id/reservations/:rid", resController.getRes);

// update a reservation
router.put("/:id/reservations/:rid", auth.validateToken, resController.put);

// delete a reservation
router.delete("/:id/reservations/:rid", auth.validateToken, resController.delete);

// export the router with different endpoints
module.exports = router;