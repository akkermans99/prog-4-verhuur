// database connection
const db = require("../database/mssql.dao");

// all defined querys and errors
const querys = require('../constants/querys');
const errors = require('../constants/errors');

// logger package
const logger = require('logger').createLogger();

module.exports = {
    // get all apartments
    getAll: (req, res, next) => {
        logger.info("getAll aangeroepen");
        let arrayApp;
        // select all apartments of the db
        db.executeQuery(querys.SELECT_ALL_APP, function(err, result) {
            if (err) {
                logger.error(err);
                next(err);
            } else {
                arrayApp = result.recordset;
                // loop through all the apartments
                for ( let i = 0; i < arrayApp.length; i++ ) {
                    // select all the reservations of that apartment
                    db.executeQuery(querys.SELECT_ALL_RESERVATIONS(arrayApp[i].ApartmentId), function (error, rows) {
                       if ( error ) {
                           logger.error(error);
                           next(error);
                       } else if ( (i+1) === arrayApp.length ) { // if the last apartment got his reservations send json with reservations
                           arrayApp[i].Reservations = rows.recordset;
                           res.status(200).json({result: arrayApp});
                       } else {
                           arrayApp[i].Reservations = rows.recordset;
                       }
                    });
                }
            }
        });

    },
    getAllWithOwner(req, res, next) {
        logger.info("getAllWithOwner called");
        db.executeQuery(querys.GET_ALL_APP_WITH_USER_WITH_RES, function (err, result) {
            if ( err ) {
                logger.error(err);
                next(err);
            } else {
                if ( result.recordset.length === 0 ) {
                    next(errors.NO_APP_FOUND);
                } else {
                    res.status(200).json(JSON.parse(result.recordset[0].Result));
                }
            }
        })
    },
    getAppWithOwner(req, res, next) {
        logger.info('getAppWithOwner called');
        let appID = req.params.id;
        db.executeQuery(querys.GET_APP_WITH_USER_WITH_RES(appID), function(err, result) {
            if ( err ) {
                logger.error(err);
                next(err);
            } else {
                if ( result.recordset.length === 0 ) {
                    next(errors.NO_APP_FOUND);
                } else {
                    res.status(200).json(JSON.parse(result.recordset[0].Result));
                }
            }
        })
    },
    // add an apartment
    post: (req, res, next) => {
        logger.info("post aangeroepen");
        let description = req.body.Description;
        let streetAddress = req.body.StreetAddress;
        let postalCode = req.body.PostalCode;
        let city = req.body.City;
        let userId = req.userId;

        // insert apartment
        db.executeQuery(querys.INSERT_APP(description, streetAddress, postalCode, city, userId), function(err, result) {
            if ( err ) {
                logger.error(err);
                next(err);
            } else {
                // get all the apartments of this user to check if apartment is added
                db.executeQuery(querys.GET_ALL_APARTMENTS_USER(userId), function(error, rows) {
                    if (error) {
                        next(error);
                    } else {
                        if ( rows.recordset.length === 0 ) {
                            next(errors.NO_APP_FOUND);
                        } else {
                            res.status(200).json({result: rows.recordset});
                        }
                    }
                });
            }
        });
    },
    // update an apartment
    put: (req, res, next) => {
        logger.info("put aangeroepen");
        let description = req.body.Description;
        let streetAddress = req.body.StreetAddress;
        let postalCode = req.body.PostalCode;
        let city = req.body.City;
        let userId = req.userId;
        let appID = req.params.id;
        let query = querys.UPDATE_APP(description, streetAddress, postalCode, city, userId, appID);

        // select the apartment
        db.executeQuery(querys.SELECT_APP(appID), function(err, result) {
            if ( err ) {
                logger.error(err);
                next(err);
            } else {
                if ( result.recordset.length === 0 ) {
                    next(errors.NO_APP_FOUND);
                } else {
                    // check if user is allowed to change this apartment
                    if ( result.recordset[0].UserId !== userId ) {
                        next(errors.NOT_ALLOWED_UPDATE_APP);
                    } else {
                        // update apartment
                        db.executeQuery(query, function(error, rows) {
                            if ( error ) {
                                logger.error(error);
                                next(error);
                            } else {
                                res.status(200).json({result: rows.recordset});
                            }
                        });
                    }
                }
            }
        })
    },
    // delete an apartment
    delete: (req, res, next) => {
        logger.info("delete aangeroepen");
        let appID = req.params.id;
        let userId = req.userId;
        // select the apartment first
        db.executeQuery(querys.SELECT_APP(appID), function (error, rows) {
            if ( error ) {
                logger.error(error);
                next(error);
            } else {
                if ( rows.recordset.length === 0 ) {
                    next(errors.NO_APP_FOUND);
                } else {
                    // check if user is allowed to delete this apartment
                    if ( rows.recordset[0].UserId !== userId ) {
                        next(errors.NOT_ALLOWED_DELETE_APP);
                    } else {
                        // delete the apartment
                        db.executeQuery(querys.DELETE_APP(appID, userId), function (err, result) {
                            if (err) {
                                logger.error(err);
                                next(err);
                            } else {
                                res.status(200).json({result: rows.recordset});
                            }
                        });
                    }
                }
            }
        });
    },
    // get 1 apartment
    getApp: (req, res, next) => {
        logger.info("getApp aangeroepen");
        let id = req.params.id;
        // select the apartment
        db.executeQuery(querys.SELECT_APP(id), function(err, result) {
            if ( err ) {
                logger.error(err);
                next(err);
            } else {
                let appArray = result.recordset[0];
                if ( result.recordset.length === 0 ) {
                    next(errors.NO_APP_FOUND);
                } else {
                    // select all the reservations of this apartment
                    db.executeQuery(querys.SELECT_ALL_RESERVATIONS(appArray.ApartmentId), function (error, rows) {
                        if ( error ) {
                            logger.error(error);
                            next(error);
                        } else {
                            appArray.Reservations = rows.recordset;
                            res.status(200).json({result: appArray});
                        }
                    });
                }
            }
        })
    }
};