// database connection
const db = require("../database/mssql.dao");

// defined querys and errors
const querys = require("../constants/querys");
const errors = require('../constants/errors');
const regexp = require('../constants/regexp');

// require all standard packages
const jwt = require('jsonwebtoken');
const logger = require('logger').createLogger();
const bcrypt = require('bcrypt');

module.exports = {
    // register user
    registerUser: (req, res, next) => {
        logger.info("registerUser aangeroepen");
        let firstName = req.body.FirstName;
        let lastName = req.body.LastName;
        let streetAddress = req.body.StreetAddress;
        let postalCode = req.body.PostalCode;
        let city = req.body.City;
        let phoneNumber = req.body.PhoneNumber;
        let emailAddress = req.body.EmailAddress;
        let dateOfBirth = req.body.DateOfBirth;
        let password = req.body.Password;

        // hash password
        let salt = bcrypt.genSaltSync(10);
        let hash = bcrypt.hashSync(password, salt);

        let fieldsValid = true;

        // check if all regex are valid
        if ( !regexp.phoneValidator(phoneNumber) ) {
            fieldsValid = false;
            next(errors.PHONENUMBER_NOT_VALID);
        }
        if ( !regexp.postalCodeValidator(postalCode) ) {
            fieldsValid = false;
            next(errors.POSTALCODE_NOT_VALID);
        }
        if ( !regexp.emailValidator(emailAddress) ) {
            fieldsValid = false;
            next(errors.EMAIL_NOT_VALID);
        }
        if ( !regexp.nameValidator(firstName) ) {
            fieldsValid = false;
            next(errors.FIRSTNAME_NOT_VALID);
        }
        if ( !regexp.nameValidator(lastName) ) {
            fieldsValid = false;
            next(errors.LASTNAME_NOT_VALID);
        }
        if ( fieldsValid ) {
            db.executeQuery(querys.REGISTER(firstName, lastName, streetAddress, postalCode, city, phoneNumber, emailAddress, dateOfBirth, hash), function(err, result) {
                if ( err ) {
                    logger.error(err);
                    next(err);
                } else {
                    logger.info("User geregistreerd");
                    res.status(200).json({result: result});
                }
            })
        }

    },
    // login user, create jwt token
    loginUser: (req, res, next) => {
        logger.info("loginUser aangeroepen");
        let emailAddress = req.body.EmailAddress;
        let password = req.body.Password;

        db.executeQuery(querys.LOGIN(emailAddress), function(err, result) {
            if ( err || result.recordset.length === 0) {
                logger.error(err);
                next(err);
            } else {
                if (result.recordset.length === 0 || !bcrypt.compareSync(password, result.recordset[0].Password)) {
                    next(errors.NO_ACCESS_EMAIL_NOT_VALID_OR_PASSWORD_NOT_CORRECT);
                } else {
                    logger.info('Password match, user logged id');
                    //logger.trace(result.recordset)

                    // Maak de payload, die we in het token stoppen.
                    // De payload kunnen we er bij het verifiëren van het token later weer uithalen.
                    const payload = {
                      UserId: result.recordset[0].UserId
                    };
                    
                    jwt.sign({ data: payload }, 'secretkey', { expiresIn: 60 * 60 * 72 }, (err, token) => {
                      if (err) {
                        next(errors.NOT_ABLE_CREATE_JWT);
                      }
                      if (token) {
                        res.status(200).json({
                          result: {
                            token: token
                          }
                        });
                      }
                    })
                }
            }
        })
    },
    // get all users
    getAllUsers: (req, res, next) => {
        logger.info('getAllUsers aangeroepen');
        db.executeQuery(querys.GET_ALL_USERS, function(err, result) {
            if ( err ) {
                logger.error(err);
                next(err);
            } else {
                res.status(200).json({result: result.recordset});
            }
        })
    }, 
    // validate jwt token
    validateToken: (req, res, next) => {
        logger.info('validateToken aangeroepen');
        // logger.debug(req.headers)
        const authHeader = req.headers.authorization;
        if (!authHeader) {
          logger.warn('Validate token failed: ', errorObject.message);
          return next(errors.AUTH_HEADER_MISSING);
        }
        const token = authHeader.substring(7, authHeader.length);
    
        jwt.verify(token, 'secretkey', (err, payload) => {
          if (err) {
            logger.warn('Validate token failed: ', errorObject.message);
            next(errors.NOT_AUTHORIZED);
          }
          //logger.trace('payload', payload)
          if (payload.data && payload.data.UserId) {
            logger.debug('token is valid', payload);
            // User heeft toegang. Voeg UserId uit payload toe aan
            // request, voor ieder volgend endpoint.
            req.userId = payload.data.UserId;
            next();
          } else {
            logger.warn('Validate token failed: ', errorObject.message);
            next(errors.USERID_MISSING);
          }
        })
      },
    
}