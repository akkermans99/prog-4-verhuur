const db = require("../database/mssql.dao");
const querys = require('../constants/querys');
const errors = require('../constants/errors');
const logger = require('logger').createLogger();

module.exports = {
    // get all reservations 
    getAllRes: (req, res, next) => {
        logger.info("getAllRes aangeroepen");
        let id = req.params.id;
        db.executeQuery(querys.SELECT_ALL_RESERVATIONS(id), function(err, result) {
            if ( err ) {
                logger.error(err);
                next(err);
            } else {
                let recordset = result.recordset;
                res.status(200).json({result: recordset});
            }
        })
    }, 
    // get a reservation
    getRes: (req, res, next) => {
        logger.info("getRes aangeroepen");
        let appID = req.params.id;
        let resID = req.params.rid;

        db.executeQuery(querys.SELECT_RESERVATION(appID, resID), function(err, result) {
            if ( err ) {
                logger.error(err);
                next(err)
            } else {
                let recordset = result.recordset;
                if ( recordset.length === 0 ) {
                    next(errors.NO_RES_FOUND);
                } else {
                    res.status(200).json({result: recordset});
                }
            }

        })
    },
    // add a reservation
    post: (req, res, next) => {
        logger.info("post aangeroepen");
        let userId = req.userId;
        let appID = req.params.id;
        let startDate = req.body.StartDate;
        let endDate = req.body.EndDate;
        let status = req.body.Status;

        // check if startdate and enddate are valid
        let check = checkPeriod(startDate, endDate);

        if ( check === errors.STARTDATE_BEFORE_CURRENT_CODE ) {
            next(errors.STARTDATE_BEFORE_CURRENT_ERROR);
        } else if ( check === errors.STARTDATE_AFTER_ENDDATE_CODE ) {
            next(errors.STARTDATE_AFTER_ENDDATE_ERROR);
        } else {
            db.executeQuery(querys.INSERT_RES(appID, startDate, endDate, status, userId), function(err, result) {
                if ( err ) {
                    logger.error(err);
                    next(err);
                } else {
                    db.executeQuery(querys.GET_ALL_RESERVATIONS_USER(userId), function(error, rows) {
                        if ( error ) {
                            logger.error(error);
                            next(error);
                        } else {
                            if ( rows.recordset.length === 0 ) {
                                logger.warn("There are no reservations for this user");
                                next(errors.NO_RESERVATIONS_ERROR_FOR_USER);
                            } else {
                                logger.info("Successfully added reservation");
                                res.status(200).json({result: rows.recordset});
                            }

                        }
                    })
                }
            })
        }

    },
    // update reservation
    put: (req, res, next) => {
        logger.info("put aangeroepen");
        let userId = req.userId;
        let appID = req.params.id;
        let resID = req.params.rid;
        let status = req.body.Status;

        db.executeQuery(querys.SELECT_APP(appID), function(err, result) {
            if ( err ) {
                logger.error(err);
                next(err);
            } else {
                if ( result.recordset.length === 0 ) {
                    next(errors.NO_APP_FOUND);
                } else {
                    if ( result.recordset[0].UserId !== userId ) {
                        next(errors.NOT_ALLOWED_UPDATE_RES);
                    } else {
                        db.executeQuery(querys.UPDATE_RES(status, appID, resID), function(error, rows) {
                            if ( error ) {
                                logger.error(error);
                                next(error);
                            } else {
                                res.status(200).json({result: rows.recordset});
                            }
                        })
                    }
                }
            }
        });
    },
    // delete reservation
    delete: (req, res, next) => {
        logger.info("delete aangeroepen");
        let appID = req.params.id;
        let resID = req.params.rid;
        let userId = req.userId;

        db.executeQuery(querys.SELECT_RESERVATION(appID, resID), function (error, rows) {
            if ( error ) {
                logger.error(error);
                next(error);
            } else {
                if ( rows.recordset.length === 0 ) {
                    next(errors.NO_RES_FOUND);
                } else {
                    if ( rows.recordset[0].UserId !== userId ) {
                        next(errors.NOT_ALLOWED_DELETE_RES);
                    } else {
                        db.executeQuery(querys.DELETE_RES(appID, resID, userId), function(err, result) {
                            if ( err) {
                                logger.error(err);
                                next(err);
                            } else {
                                logger.info("Deleted reservation");
                                res.status(200).json({result: rows.recordset});
                            }
                        })
                    }
                }
            }
        })

    }
};


/**
 * check if the startdate is before current date and enddate
 * @param startDate
 * @param endDate
 * @returns {boolean|number}
 */
function checkPeriod(startDate, endDate) {
    let currentDate = new Date();
    startDate = new Date(startDate);
    endDate = new Date(endDate);

    if ( startDate > currentDate ) {
        if ( startDate >= endDate ) {
            return errors.STARTDATE_AFTER_ENDDATE_CODE;
        } else {
            return true;
        }
    } else {
        return errors.STARTDATE_BEFORE_CURRENT_CODE;
    }
}