// db configuration setings
const config = require('../config/config');
const dbconfig = config.dbconfig;
const serverconfig = config.serverconfig;

// sql package
const sql = require('mssql');

// logger
const logger = require('logger').createLogger();


module.exports = {
  // execute query and call callback
  async executeQuery(query, callback) {
    const pool = new sql.ConnectionPool(serverconfig);
    pool.on('error', err => {
      logger.error(err);
    });

    try {
      await pool.connect();
      const result = await pool.request().query(query);
      callback(null, result);
    } catch(error) {
      logger.error(error);
      callback(error, null);
    } finally {
      pool.close();
    }
  }
};

