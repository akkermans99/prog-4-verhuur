// test packages
const chai = require('chai');
const chaiHttp = require('chai-http');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

// LET OP: voeg onder aan app.js toe: module.exports = app
const server = require('../index');
const querys = require('../src/constants/querys');
const regexp = require('../src/constants/regexp');
const db = require('../src/database/mssql.dao');

chai.should();
chai.use(chaiHttp);

const endpointToTest = '/api/appartments';
const authorizationHeader = 'Authorization';
let token;

let testObj = {
    userId: 1,
    appID: 1243,
    resID: 875,
    city: 'Breda',
    postal: '4444 RR',
    street: 'Ziekenhuisstraat 1',
    descr: 'Ziekenhuis',
    startdate: '2019-05-25',
    enddate: '2019-05-28',
    status: 'ACCEPTED'
};

//
// Deze functie wordt voorafgaand aan alle tests 1 x uitgevoerd.
//
before(() => {
  console.log('- before: get valid token');
  // We hebben een token nodig om een ingelogde gebruiker te simuleren.
  // Hier kiezen we ervoor om een token voor UserId 1 te gebruiken.
  const payload = {
      UserId: 1
  };
  jwt.sign({ data: payload }, 'secretkey', { expiresIn: 2 * 60 }, (err, result) => {
    if (result) {
      token = result;
    }
  })


});

beforeEach(() => {
  console.log('- beforeEach');
});

// AUTHENTICATION

/*describe('Register test', () => {
    it('should register an user and add it to the db', done=> {
        let email = "test123456@gmail.com";
        let password = "test";
        chai
            .request(server)
            .post("/auth/register")
            .send({
                FirstName: "Test",
                LastName: "Testing",
                StreetAddress: "teststraat 123",
                PostalCode: "3434 AB",
                City: "Teststad",
                DateOfBirth: "2000-01-01",
                PhoneNumber: "0622334455",
                EmailAddress: email,
                Password: password
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.a('object');
                testObj.email = email;
                testObj.password = password;
               done();
            });
    })
});*/

describe("Authentication get all users", () => {
    it('should get all the users', done => {
        chai
            .request(server)
            .get("/auth/users")
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.a('object');
                done();
            });
    });
});


describe('Login test', ()=> {
   it('should login the user and generate jwt', done=> {
      chai.request(server)
          .post("/auth/login")
          .send({
              EmailAddress: "test123456@gmail.com",
              Password: "test"
          })
          .end((err, res) => {
              res.should.be.status(200);
              res.should.be.a('object');
              const result = res.body.result;
              result.should.be.a('object');
              const rtoken = result.token;
              jwt.verify(rtoken, 'secretkey', (err, payload) => {
                  payload.data.should.be.a('object');
                  payload.data.should.have.property('UserId');
                  done();
              });
          });
   });
});


// APARTMENTS
describe('Apartments API POST an appartment', () => {
    it('should add an apartment to the database', done => {
        let descr = 'Katjeskelder';
        let street = 'Vijf eikenweg 34';
        let postal = '5000 AD';
        let city = 'Rijen';
        chai
            .request(server)
            .post(endpointToTest)
            .set(authorizationHeader, 'Bearer ' + token)
            .send({
                Description: descr,
                StreetAddress: street,
                PostalCode: postal,
                City: city
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.a('object');
                done();
            })
    })
});

describe('Apartments API GET ALL', () => {
  it('should return an array of Apartments', done => {
    chai
        .request(server)
        .get(endpointToTest)
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            done();
        })
  })
});


describe('Apartments API GET an appartment', () => {
    it('should return the apartment ', done => {
      chai
          .request(server)
          .get(endpointToTest+"/"+testObj.appID)
          .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('array');
              const result = res.body[0];
              result.should.be.an('object');
              result.should.have.property('ApartmentId').equals(testObj.appID);
              result.should.have.property('Description').equals(testObj.descr);
              result.should.have.property('StreetAddress').equals(testObj.street);
              result.should.have.property('PostalCode').equals(testObj.postal);
              result.should.have.property('City').equals(testObj.city);
              result.should.have.property('UserId').equals(testObj.userId);
              result.should.have.property('Reservations').should.be.a('object');
              done();
          })
    })
  });





describe('Apartments API PUT an appartment', () => {
    it('should update an apartment', done => {
        let description = "Ziekenhuis";
        let streetAddress = "Ziekenhuisstraat 1";
        let postalcode = "4444 RR";
        let city = "Breda";
        chai
            .request(server)
            .put(endpointToTest+"/"+testObj.appID)
            .set(authorizationHeader, 'Bearer ' + token)
            .send({
                Description: description,
                StreetAddress: streetAddress,
                PostalCode: postalcode,
                City: city
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            })
    })
});




  // RESERVATIONS
describe('Reservation API POST a reservation', () => {
    it('should add a reservation to the database', done => {
        let startdate = "2019-06-14";
        let enddate = "2019-06-17";
        let status = 'INITIAL';
        chai
            .request(server)
            .post(endpointToTest+"/"+ testObj.appID +"/reservations")
            .set(authorizationHeader, 'Bearer ' + token)
            .send({
                StartDate: startdate,
                EndDate: enddate,
                Status: status
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.a('object');
                done();
            })
    })
});

  describe('Reservations API GET ALL', () => {
    it('should return all the reservations', done => {
      chai
          .request(server)
          .get(endpointToTest+"/"+testObj.appID+"/reservations")
          .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              const result = res.body.result;
              result.should.be.an('array');
              done();
          })
    })
  });

  describe('Reservations API GET 1 reservation', () => {
    it('should return the reservation', done => {
      chai
          .request(server)
          .get(endpointToTest+"/"+testObj.appID+"/reservations/"+testObj.resID)
          .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              const result = res.body.result;
              result.should.be.an('array');
              const apartment = result[0];
              apartment.should.be.a('object');
              apartment.should.have.property('ApartmentId').equals(testObj.appID);
              apartment.should.have.property('StartDate').equals(testObj.startdate+'T00:00:00.000Z');
              apartment.should.have.property('EndDate').equals(testObj.enddate+'T00:00:00.000Z');
              apartment.should.have.property('Status').equals(testObj.status);
              apartment.should.have.property('ReservationId').equals(testObj.resID);
              apartment.should.have.property('UserId').equals(testObj.userId);
              done();
          })
    })
  });





describe('Reservation API PUT a reservation', () => {
    it('should update a reservation', done => {
        chai
            .request(server)
            .put(endpointToTest+"/"+testObj.appID+"/reservations/"+testObj.resID)
            .set(authorizationHeader, 'Bearer ' + token)
            .send({
                Status: "ACCEPTED"
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            })
    })
});


/*describe('Reservation API DELETE a reservation', () => {
    it('should delete a reservation  and returns the delete reservation', done => {
        chai
            .request(server)
            .delete(endpointToTest+"/"+testObj.appID+"/reservations/"+testObj.resID)
            .set(authorizationHeader, 'Bearer ' + testObj.token)
            .end((err, res) => {
                res.should.have.status(200);
                res.should.be.a('object');
                const result = res.body.result;
                result.should.be.an('array');
                const reservation = result[0];
                reservation.should.have.property('UserId').equals(testObj.userId);
                reservation.should.have.property('ReservationId').equals(testObj.resID);
                done();
            })
    })
});

describe('Apartment API DELETE ', () => {
    it('should delete apartment from the database and return the delete apartment', done => {
        chai
            .request(server)
            .delete(endpointToTest+"/"+testObj.appID)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                const result = res.body.result;
                result.should.be.an('array');
                const app = result[0];
                app.should.have.property('UserId').equals(testObj.userId);
                app.should.have.property('ReservationId').equals(testObj.resID);
                done();
            });
    })
});*/



// QUERYS
describe("Query post reservation", () => {
    it("should give back a string with the insert query with every values filled in", done => {
        const appID = 1;
        const userId = 1;
        const startDate = "2019-05-19";
        const endDate = "2019-07-19";
        const status = "INITIAL";

        let query = querys.INSERT_RES(appID, startDate, endDate, status, userId);
        query.should.be.a('string').equals(`insert into Reservation (ApartmentId, StartDate, EndDate, Status, UserId) values (${appID}, '${startDate}', '${endDate}', '${status}', ${userId})`);
        done();
    })
});

describe("Name regex nameValidator", ()=>{
    it('should return true when send Piet, should return false when send Piet1', done=> {
        const valid = 'Piet';
        const invalid = 'Piet1';
        let validRes = regexp.nameValidator(valid);
        let invalidRes = regexp.nameValidator(invalid);
        let emptyRes = regexp.nameValidator('        ');
        let undefinedRes = regexp.nameValidator(null);
        validRes.should.be.equals(true);
        invalidRes.should.be.equals(false);
        emptyRes.should.be.equals(false);
        undefinedRes.should.be.equals(false);
        done();
    });
});

describe("email regex emailValidator", () => {
    it('should return true when email is OK, else false when incorrect', done=>{
       const valid = 'test@gmail.com';
       const invalid = 'test.gmail.com';
       const empty = '    ';
       const nullVal = null;

       regexp.emailValidator(valid).should.be.equals(true);
       regexp.emailValidator(invalid).should.be.equals(false);
       regexp.emailValidator(empty).should.be.equals(false);
       regexp.emailValidator(nullVal).should.be.equals(false);
       done();
    });
});

describe('phonenumber regex phoneValidator', () => {
    it('should return true when email is OK, else false when incorrect', done=>{
        const valid = '0645454545';
        const invalid = '064545454';
        const empty = '    ';
        const nullVal = null;

        regexp.phoneValidator(valid).should.be.equals(true);
        regexp.phoneValidator(invalid).should.be.equals(false);
        regexp.phoneValidator(empty).should.be.equals(false);
        regexp.phoneValidator(nullVal).should.be.equals(false);
        done();
    });
});

describe('postalcode regex postalCodeValidator', () => {
    it('should return true when email is OK, else false when incorrect', done=>{
        const valid = '4545 AA';
        const invalid = '434 ABA';
        const empty = '    ';
        const nullVal = null;

        regexp.postalCodeValidator(valid).should.be.equals(true);
        regexp.postalCodeValidator(invalid).should.be.equals(false);
        regexp.postalCodeValidator(empty).should.be.equals(false);
        regexp.postalCodeValidator(nullVal).should.be.equals(false);
        done();
    });
});