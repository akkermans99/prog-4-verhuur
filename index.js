// including needed modules
const express = require('express');
const mssql = require("./src/database/mssql.dao");
const bodyParser = require('body-parser');
const querys = require('./src/constants/querys');
const logger = require('logger').createLogger();

// including routes modules
const appRoutes = require("./src/routes/appartments.router");
const authRoutes = require("./src/routes/auth.router");

// create app 
const app = express();

// let the app listen to a port
let port = process.env.PORT || 3500;
app.listen(port);
console.log("Listening to port", port);

// add bodyparser to the app
app.use(bodyParser.json());

// route to differend end points
app.use("/api/appartments", appRoutes);
app.use("/auth", authRoutes);

// Generic endpoint handler - voor alle routes
app.all('*', (req, res, next) => {
    // logger.info('Generieke afhandeling aangeroepen!')
    // ES16 deconstructuring
    const { method, url } = req;
    logger.info(`${method} ${url}`);
    next();
  });
  

// Handle endpoint not found.
app.all('*', (req, res, next) => {
    const { method, url } = req;
    const errorMessage = `${method} ${url} does not exist.`;
    logger.warn(errorMessage);
    const errorObject = {
      message: errorMessage,
      code: 404,
      date: new Date()
    };
    next(errorObject);
  });
  
  // Error handler
  app.use((error, req, res, next) => {
    logger.error('Error handler: ', error.message.toString());
    res.status(error.code).json(error);
  });
  

module.exports = app;